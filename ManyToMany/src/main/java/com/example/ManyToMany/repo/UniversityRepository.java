package com.example.ManyToMany.repo;

import com.example.ManyToMany.models.University;
import org.springframework.data.repository.CrudRepository;

public interface UniversityRepository extends CrudRepository<University, Long> {
    University findByName(String name);
}
