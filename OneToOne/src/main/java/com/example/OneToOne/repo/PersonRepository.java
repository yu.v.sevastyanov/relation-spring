package com.example.OneToOne.repo;

import com.example.OneToOne.models.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
