package com.example.OneToOne.controller;


import com.example.OneToOne.models.Pasport;
import com.example.OneToOne.models.Person;
import com.example.OneToOne.repo.PasportRepository;
import com.example.OneToOne.repo.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class MainController {
    @Autowired
    private PasportRepository pasportRepository;
    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/person")
    public String Main(Model model){
        Iterable<Pasport> pasport = pasportRepository.findAll();
        ArrayList<Pasport> pasportArrayList = new ArrayList<>();
        for (Pasport pass: pasport) {
            if (pass.getOwner() == null) {
                pasportArrayList.add(pass);
            }
        }

        model.addAttribute("pasport", pasport);
        return "person";
    }

    @PostMapping("/person/add")
    public String blogPostAdd(@RequestParam String name, @RequestParam String number, Model model)
    {
        System.out.println(name);
        Pasport pasport = pasportRepository.findByNumber(number);
        System.out.println(pasport.getId());
        Person person = new Person(name, pasport);
        personRepository.save(person);
        return "redirect:/person";
    }
}
