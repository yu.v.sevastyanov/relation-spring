package com.example.OneToManyAndManyToOne.repo;

import com.example.OneToManyAndManyToOne.models.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
    Address findByStreet(String street);
}
