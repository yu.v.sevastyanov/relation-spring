package com.example.OneToManyAndManyToOne.repo;

import com.example.OneToManyAndManyToOne.models.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
